var gulp = require('gulp');
var plumber=require('gulp-plumber');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');


gulp.task('sass', function (){
  gulp.src('scss/*.scss')
    .pipe(plumber())
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('css'));
  });

gulp.task('uglify',function(){
  return gulp.src('eventos/*.js')
    .pipe(plumber())
    .pipe(uglify())
    .pipe(gulp.dest('dist'));
});

gulp.task('sass:watch', function () {
  gulp.watch('scss/*.scss'),('/*.js'), ['sass'],['uglify']);
});
